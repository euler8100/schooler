module.exports = {
    publicPath: "",
    devServer: {
        disableHostCheck: true,
        compress: true,
        port: 3008,
    }
}
