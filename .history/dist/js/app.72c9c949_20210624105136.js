(function(e) {
  function t(t) {
    for (
      var r, i, u = t[0], o = t[1], c = t[2], l = 0, p = [];
      l < u.length;
      l++
    )
      (i = u[l]),
        Object.prototype.hasOwnProperty.call(n, i) && n[i] && p.push(n[i][0]),
        (n[i] = 0);
    for (r in o) Object.prototype.hasOwnProperty.call(o, r) && (e[r] = o[r]);
    d && d(t);
    while (p.length) p.shift()();
    return a.push.apply(a, c || []), s();
  }
  function s() {
    for (var e, t = 0; t < a.length; t++) {
      for (var s = a[t], r = !0, u = 1; u < s.length; u++) {
        var o = s[u];
        0 !== n[o] && (r = !1);
      }
      r && (a.splice(t--, 1), (e = i((i.s = s[0]))));
    }
    return e;
  }
  var r = {},
    n = { app: 0 },
    a = [];
  function i(t) {
    if (r[t]) return r[t].exports;
    var s = (r[t] = { i: t, l: !1, exports: {} });
    return e[t].call(s.exports, s, s.exports, i), (s.l = !0), s.exports;
  }
  (i.m = e),
    (i.c = r),
    (i.d = function(e, t, s) {
      i.o(e, t) || Object.defineProperty(e, t, { enumerable: !0, get: s });
    }),
    (i.r = function(e) {
      "undefined" !== typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(e, "__esModule", { value: !0 });
    }),
    (i.t = function(e, t) {
      if ((1 & t && (e = i(e)), 8 & t)) return e;
      if (4 & t && "object" === typeof e && e && e.__esModule) return e;
      var s = Object.create(null);
      if (
        (i.r(s),
        Object.defineProperty(s, "default", { enumerable: !0, value: e }),
        2 & t && "string" != typeof e)
      )
        for (var r in e)
          i.d(
            s,
            r,
            function(t) {
              return e[t];
            }.bind(null, r)
          );
      return s;
    }),
    (i.n = function(e) {
      var t =
        e && e.__esModule
          ? function() {
              return e["default"];
            }
          : function() {
              return e;
            };
      return i.d(t, "a", t), t;
    }),
    (i.o = function(e, t) {
      return Object.prototype.hasOwnProperty.call(e, t);
    }),
    (i.p = "");
  var u = (window["webpackJsonp"] = window["webpackJsonp"] || []),
    o = u.push.bind(u);
  (u.push = t), (u = u.slice());
  for (var c = 0; c < u.length; c++) t(u[c]);
  var d = o;
  a.push([0, "chunk-vendors"]), s();
})({
  0: function(e, t, s) {
    e.exports = s("56d7");
  },
  "01d1": function(e, t, s) {},
  "034f": function(e, t, s) {
    "use strict";
    s("85ec");
  },
  "0bdb": function(e, t, s) {},
  "0c66": function(e, t, s) {
    "use strict";
    s("0bdb");
  },
  1771: function(e, t, s) {
    s("07ac");
    var r = s("970b"),
      n = s("5bc3"),
      a = (function() {
        "use strict";
        function e() {
          r(this, e);
        }
        return (
          n(e, null, [
            {
              key: "isEmpty",
              value: function(e) {
                if (!e) return !0;
                if (Array.isArray(e)) return 0 === e.length;
                var t = Object.values(e);
                return t && 0 == t.length;
              }
            }
          ]),
          e
        );
      })();
    t.Commons = a;
  },
  1885: function(e, t, s) {
    "use strict";
    s("b7da");
  },
  "266e": function(e, t, s) {},
  3565: function(e, t, s) {},
  "36e8": function(e, t, s) {},
  "3e82": function(e, t, s) {},
  "4d4b": function(e, t, s) {
    "use strict";
    s("85f4");
  },
  "50a7": function(e, t, s) {
    "use strict";
    s("3565");
  },
  "54f3": function(e, t, s) {
    e.exports = s.p + "img/x.9ebf52be.svg";
  },
  "56d7": function(e, t, s) {
    "use strict";
    s.r(t);
    s("e260"), s("e6cf"), s("cca6"), s("a79d");
    var r = s("2b0e"),
      n = s("5f5b"),
      a = s("b1e0"),
      i = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s(
          "div",
          { staticClass: "align-center appContainer baseElement" },
          [
            s("Header"),
            s(
              "transition",
              { attrs: { name: "fade" } },
              [s("router-view", { staticClass: "pagesContainer" })],
              1
            ),
            s("Footer")
          ],
          1
        );
      },
      u = [],
      o = (s("96cf"), s("1da1")),
      c = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s("div", [
          s("div", { staticClass: "appHeader" }, [e._v("Schooler")]),
          e.showNavBar
            ? s(
                "div",
                { staticClass: "navBar" },
                [
                  s("NavBtn", {
                    attrs: { path: "/publications", name: "Publications" }
                  }),
                  s("NavBtn", {
                    attrs: { path: "/users", name: "Utilisateurs" }
                  }),
                  s("NavBtn", {
                    attrs: { path: "/messages", name: "Messages" }
                  })
                ],
                1
              )
            : e._e()
        ]);
      },
      d = [],
      l = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s(
          "router-link",
          { staticClass: "d-inline-block m-2", attrs: { to: e.path } },
          [
            s("span", { staticClass: "btn d-inline-block" }, [
              e._v(e._s(e.name))
            ]),
            s("br"),
            s("div", { staticClass: "sepatation" })
          ]
        );
      },
      p = [],
      m = { name: "NavBtn", props: { path: String, name: String } },
      f = m,
      v = (s("b9f3"), s("2877")),
      h = Object(v["a"])(f, l, p, !1, null, "50309336", null),
      g = h.exports,
      b = {
        name: "Header",
        components: { NavBtn: g },
        data: function() {
          return { showNavBar: !1 };
        },
        mounted: function() {
          (this.showNavBar =
            (window.schooler.userData &&
              window.schooler.userData.userConnected) ||
            !1),
            this.userConnected || this.$router.push("/"),
            setInterval(
              function(e) {
                e.showNavBar =
                  (window.schooler.userData &&
                    window.schooler.userData.userConnected &&
                    !window.schooler.userData.hideNavBar) ||
                  !1;
              },
              250,
              this
            );
        }
      },
      w = b,
      D = (s("50a7"), Object(v["a"])(w, c, d, !1, null, "e6468046", null)),
      _ = D.exports,
      N = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s(
          "div",
          {
            staticClass: "text-muted author position-fixed p-2 hover_pointer",
            attrs: { title: "Entreprise technologique" }
          },
          [e._v(" By Chipdeals Inc. ")]
        );
      },
      C = [],
      P = { name: "Footer" },
      U = P,
      x = (s("760c"), Object(v["a"])(U, N, C, !1, null, null, null)),
      J = x.exports,
      G = s("bc3a"),
      S = s.n(G),
      M = {
        name: "App",
        scripts: ["https://cdn.branch.io/branch-latest.min.js"],
        components: { Header: _, Footer: J },
        data: function() {
          return {};
        },
        mounted: function() {
          return Object(o["a"])(
            regeneratorRuntime.mark(function e() {
              return regeneratorRuntime.wrap(function(e) {
                while (1)
                  switch ((e.prev = e.next)) {
                    case 0:
                      window.schooler.userData = {
                        user: {},
                        userConnected: !1
                      };
                    case 1:
                    case "end":
                      return e.stop();
                  }
              }, e);
            })
          )();
        },
        methods: {
          getDataFrom: function(e) {
            var t =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : "get",
              s =
                arguments.length > 2 && void 0 !== arguments[2]
                  ? arguments[2]
                  : null,
              r =
                arguments.length > 3 && void 0 !== arguments[3]
                  ? arguments[3]
                  : null,
              n = { url: e, method: t };
            return (
              r && (n.data = r),
              s && (n.params = s),
              S.a
                .request(n)
                .then(function(e) {
                  return e.data;
                })
                .catch(function(e) {
                  console.error(e.message);
                })
            );
          }
        }
      },
      y = M,
      k = (s("034f"), Object(v["a"])(y, i, u, !1, null, null, null)),
      j = k.exports,
      O = (s("ab8b"), s("2dd8"), s("8c4f")),
      E = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s("div", { staticClass: "container" }, [
          s("h1", { staticClass: "pageTitle p-3" }, [e._v("Connection")]),
          s("br"),
          s("form", [
            s("label", { attrs: { for: "userPseudo" } }, [
              e._v("Pseudo d'utilisateur")
            ]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.userPseudo,
                  expression: "userPseudo"
                }
              ],
              staticClass: "form-control",
              domProps: { value: e.userPseudo },
              on: {
                input: function(t) {
                  t.target.composing || (e.userPseudo = t.target.value);
                }
              }
            }),
            s("br"),
            s("label", { attrs: { for: "userPassword" } }, [
              e._v("Mot de passe")
            ]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.userPassword,
                  expression: "userPassword"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "password" },
              domProps: { value: e.userPassword },
              on: {
                input: function(t) {
                  t.target.composing || (e.userPassword = t.target.value);
                }
              }
            })
          ]),
          s("br"),
          s(
            "button",
            {
              staticClass: "btn p-2 schooler_btn",
              on: {
                click: function(t) {
                  return e.logIn();
                }
              }
            },
            [e._v("Se connecter")]
          ),
          s("br"),
          s(
            "span",
            { staticClass: "signupText" },
            [
              e._v("N'avez vous pas encore de compte? "),
              s("router-link", { attrs: { to: "/signup" } }, [
                e._v("S'inscrire")
              ])
            ],
            1
          )
        ]);
      },
      q = [],
      T = {
        name: "Login",
        data: function() {
          return { userPseudo: "", userPassword: "" };
        },
        mounted: function() {
          window.schooler.userData &&
            window.schooler.userData.userConnected &&
            this.$router.push("/publications");
        },
        methods: {
          logIn: function() {
            (window.schooler.userData.user = {
              fullName: "John Doe Géraud",
              role: "3eme année licence",
              uuid: "GéraudUuid",
              picture: 1
            }),
              (window.schooler.userData.userConnected = !0),
              console.log(window.schooler.userData),
              this.$router.push("/publications");
          }
        }
      },
      B = T,
      $ =
        (s("da41"),
        s("0c66"),
        Object(v["a"])(B, E, q, !1, null, "11064b87", null)),
      L = $.exports,
      W = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s("div", { staticClass: "container" }, [
          s("h1", { staticClass: "pageTitle p-3" }, [e._v("Inscription")]),
          s("br"),
          s("form", [
            s("label", { attrs: { for: "fullName" } }, [e._v("Nom Complet")]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.fullName,
                  expression: "fullName"
                }
              ],
              staticClass: "form-control",
              domProps: { value: e.fullName },
              on: {
                input: function(t) {
                  t.target.composing || (e.fullName = t.target.value);
                }
              }
            }),
            s("br"),
            s("label", { attrs: { for: "userPseudo" } }, [
              e._v("Pseudo d'utilisateur")
            ]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.userPseudo,
                  expression: "userPseudo"
                }
              ],
              staticClass: "form-control",
              domProps: { value: e.userPseudo },
              on: {
                input: function(t) {
                  t.target.composing || (e.userPseudo = t.target.value);
                }
              }
            }),
            s("br"),
            s("label", { attrs: { for: "userPassword" } }, [
              e._v("Mot de passe")
            ]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.userPassword,
                  expression: "userPassword"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "password" },
              domProps: { value: e.userPassword },
              on: {
                input: function(t) {
                  t.target.composing || (e.userPassword = t.target.value);
                }
              }
            }),
            s("br"),
            s("label", { attrs: { for: "userPassword" } }, [
              e._v("Confirmation du mot de passe")
            ]),
            s("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: e.confPassword,
                  expression: "confPassword"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "password" },
              domProps: { value: e.confPassword },
              on: {
                input: function(t) {
                  t.target.composing || (e.confPassword = t.target.value);
                }
              }
            }),
            s("br"),
            s("label", { attrs: { for: "userPseudo" } }, [
              e._v("Quel type d'utilisateur êtes vous?")
            ]),
            s(
              "select",
              {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: e.typeUser,
                    expression: "typeUser"
                  }
                ],
                staticClass: "form-control",
                on: {
                  change: function(t) {
                    var s = Array.prototype.filter
                      .call(t.target.options, function(e) {
                        return e.selected;
                      })
                      .map(function(e) {
                        var t = "_value" in e ? e._value : e.value;
                        return t;
                      });
                    e.typeUser = t.target.multiple ? s : s[0];
                  }
                }
              },
              [
                s("option", { attrs: { value: "etudiant" } }, [
                  e._v("Etudiant")
                ])
              ]
            )
          ]),
          s("br"),
          s(
            "button",
            {
              staticClass: "btn p-2 schooler_btn",
              on: {
                click: function(t) {
                  return e.logIn();
                }
              }
            },
            [e._v("Se connecter")]
          ),
          s("br"),
          s(
            "span",
            { staticClass: "signupText" },
            [
              e._v(" Avez vous déja un compte? "),
              s("router-link", { attrs: { to: "/" } }, [e._v("Se connecter")])
            ],
            1
          )
        ]);
      },
      R = [],
      z = {
        name: "Login",
        data: function() {
          return { userPseudo: "", userPassword: "" };
        },
        mounted: function() {
          window.schooler.userData &&
            window.schooler.userData.userConnected &&
            this.$router.push("/publications");
        },
        methods: {
          signUp: function() {
            this.$router.push("/");
          }
        }
      },
      I = z,
      F =
        (s("765b"),
        s("8060"),
        Object(v["a"])(I, W, R, !1, null, "f6c76fe2", null)),
      H = F.exports,
      A = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s(
          "div",
          e._l(e.publications, function(e, t) {
            return s("Publication", { key: t, attrs: { publication: e } });
          }),
          1
        );
      },
      Y = [],
      V = function() {
        var e = this,
          t = e.$createElement,
          r = e._self._c || t;
        return r("div", [
          r(
            "div",
            {
              class:
                "m-3 p-2 publication" +
                (e.reductedPublication ? " reduced" : "")
            },
            [
              r("div", { staticClass: "p-2 publication-header" }, [
                r("div", { staticClass: "userPicture d-inline-block" }, [
                  r("img", {
                    attrs: {
                      src: s("a278")(
                        "./userPicture_" + e.publication.sender.picture + ".png"
                      )
                    }
                  })
                ]),
                r("div", { staticClass: "fullName d-inline-block m-2" }, [
                  r("span", { staticClass: "mt-2" }, [
                    e._v(e._s(e.publication.sender.fullName))
                  ]),
                  r("br"),
                  r("span", { staticClass: "text-muted" }, [
                    e._v(e._s(e.publication.sender.role))
                  ])
                ]),
                r(
                  "div",
                  { staticClass: "messageDetails d-inline-block text-right" },
                  [
                    r("span", { staticClass: "mt-2 text-muted" }, [
                      e._v(e._s(e.sendingDate))
                    ]),
                    r("br"),
                    r(
                      "span",
                      {
                        staticClass: "hover_pointer",
                        on: {
                          click: function(t) {
                            return e.tooglePublicationReduction();
                          }
                        }
                      },
                      [
                        e._v(
                          " " +
                            e._s(e.reductedPublication ? "Plus" : "Réduire") +
                            " "
                        )
                      ]
                    )
                  ]
                )
              ]),
              r("div", { staticClass: "p-2" }, [
                e._v(e._s(e.publication.message))
              ]),
              e.havePuctures
                ? r("div", { staticClass: "publicationPicture" }, [
                    r("img", { attrs: { src: s("742e") } })
                  ])
                : e._e()
            ]
          )
        ]);
      },
      Q = [],
      Z = s("1771"),
      K = Z.Commons,
      X = {
        name: "Publication",
        props: { publication: Object },
        data: function() {
          return { sendingDate: "", havePuctures: !1, reductedPublication: !0 };
        },
        mounted: function() {
          (this.sendingDate = this.getStringDate(this.publication.at)),
            (this.havePuctures = !K.isEmpty(this.publication.picturesLinks));
        },
        methods: {
          getStringDate: function(e) {
            var t = new Date(e);
            return (
              t.getDate() +
              "/" +
              t.getMonth() +
              "/" +
              t.getFullYear() +
              " " +
              t.getHours() +
              "h" +
              t.getMinutes()
            );
          },
          tooglePublicationReduction: function() {
            this.reductedPublication = !this.reductedPublication;
          }
        }
      },
      ee = X,
      te = (s("f800"), Object(v["a"])(ee, V, Q, !1, null, "5df0726b", null)),
      se = te.exports,
      re = s("1771"),
      ne = re.Commons,
      ae = {
        name: "Publications",
        components: { Publication: se },
        props: {},
        data: function() {
          return { publications: [] };
        },
        mounted: function() {
          this.loadPublications(),
            setInterval(
              function(e) {
                e.textWriten = !ne.isEmpty(e.newMessageText);
              },
              500,
              this
            );
        },
        methods: {
          loadPublications: function() {
            if (
              window.schooler.userData &&
              !ne.isEmpty(window.schooler.userData.publications)
            )
              return (
                console.log("publications already exist"),
                void (this.publications = window.schooler.userData.publications)
              );
            var e = s("64e6"),
              t = e.sort(function(e, t) {
                return t.at - e.at;
              });
            (window.schooler.userData.publications = t),
              (this.publications = window.schooler.userData.publications);
          }
        }
      },
      ie = ae,
      ue = Object(v["a"])(ie, A, Y, !1, null, "7710ac25", null),
      oe = ue.exports,
      ce = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s(
          "div",
          { staticClass: "pb-2" },
          e._l(e.users, function(e, t) {
            return s("User", { key: t, attrs: { userData: e } });
          }),
          1
        );
      },
      de = [],
      le = function() {
        var e = this,
          t = e.$createElement,
          r = e._self._c || t;
        return r("div", { staticClass: "user m-3" }, [
          r(
            "div",
            {
              staticClass: "userPicture d-inline-block hover_pointer",
              attrs: { title: "Plus de détail sur " + e.userData.fullName }
            },
            [
              e.showPuctures
                ? r("img", {
                    attrs: {
                      src: s("a278")(
                        "./userPicture_" + e.userData.picture + ".png"
                      )
                    }
                  })
                : e._e()
            ]
          ),
          r("div", { staticClass: "fullName d-inline-block m-2" }, [
            r("span", { staticClass: "mt-2" }, [
              e._v(e._s(e.userData.fullName))
            ]),
            r("br"),
            r("span", { staticClass: "text-muted" }, [
              e._v(e._s(e.userData.role))
            ])
          ]),
          r(
            "div",
            {
              staticClass: "watchMessages d-inline-block hover_pointer",
              attrs: { title: "Envoyer un message à " + e.userData.fullName }
            },
            [e.showPuctures ? r("img", { attrs: { src: s("f83a") } }) : e._e()]
          )
        ]);
      },
      pe = [],
      me = {
        name: "User",
        props: { userData: Object },
        data: function() {
          return { showPuctures: !1 };
        },
        mounted: function() {
          setTimeout(
            function(e) {
              e.showPuctures = !0;
            },
            100,
            this
          );
        }
      },
      fe = me,
      ve = (s("4d4b"), Object(v["a"])(fe, le, pe, !1, null, "5d6469a3", null)),
      he = ve.exports,
      ge = s("1771"),
      be = ge.Commons,
      we = {
        name: "Users",
        components: { User: he },
        data: function() {
          return { users: [] };
        },
        mounted: function() {
          this.loadUsers();
        },
        methods: {
          loadUsers: (function() {
            var e = Object(o["a"])(
              regeneratorRuntime.mark(function e() {
                return regeneratorRuntime.wrap(
                  function(e) {
                    while (1)
                      switch ((e.prev = e.next)) {
                        case 0:
                          if (
                            !window.schooler.userData ||
                            be.isEmpty(window.schooler.users)
                          ) {
                            e.next = 4;
                            break;
                          }
                          return (
                            console.log("users already exist"),
                            (this.users = window.schooler.users),
                            e.abrupt("return")
                          );
                        case 4:
                          (window.schooler.users = s("a638")),
                            (this.users = window.schooler.users);
                        case 6:
                        case "end":
                          return e.stop();
                      }
                  },
                  e,
                  this
                );
              })
            );
            function t() {
              return e.apply(this, arguments);
            }
            return t;
          })()
        }
      },
      De = we,
      _e = Object(v["a"])(De, ce, de, !1, null, "0ebc73a3", null),
      Ne = _e.exports,
      Ce = function() {
        var e = this,
          t = e.$createElement,
          r = e._self._c || t;
        return r("div", { staticClass: "pb-2" }, [
          e.showMessages
            ? r(
                "div",
                [
                  r("div", { staticClass: "userDescription" }, [
                    r(
                      "div",
                      {
                        staticClass: "d-inline-block return m-2 hover_pointer",
                        on: {
                          click: function(t) {
                            return e.switchMessageShowing();
                          }
                        }
                      },
                      [r("img", { attrs: { src: s("54f3") } })]
                    ),
                    r("div", { staticClass: "d-inline-block m-2" }, [
                      r("img", {
                        attrs: {
                          src: s("a278")(
                            "./userPicture_" +
                              e.conversationToShow.conversationWith.picture +
                              ".png"
                          )
                        }
                      })
                    ]),
                    r("div", { staticClass: "d-inline-block" }, [
                      e._v(e._s(e.conversationToShow.conversationWith.fullName))
                    ])
                  ]),
                  r("ConversationMessages", {
                    attrs: { conversation: e.conversationToShow }
                  })
                ],
                1
              )
            : r(
                "div",
                e._l(e.conversations, function(t, s) {
                  return r("div", { key: s }, [
                    r(
                      "div",
                      {
                        on: {
                          click: function(t) {
                            return e.showConversation(s);
                          }
                        }
                      },
                      [r("Conversations", { attrs: { conversationData: t } })],
                      1
                    )
                  ]);
                }),
                0
              )
        ]);
      },
      Pe = [],
      Ue = function() {
        var e = this,
          t = e.$createElement,
          r = e._self._c || t;
        return r("div", { staticClass: "user m-3 hover_pointer" }, [
          r("div", { staticClass: "userPicture d-inline-block" }, [
            e.showPuctures
              ? r("img", {
                  attrs: {
                    src: s("a278")(
                      "./userPicture_" +
                        e.conversationData.conversationWith.picture +
                        ".png"
                    )
                  }
                })
              : e._e()
          ]),
          r("div", { staticClass: "fullName d-inline-block m-2" }, [
            r("span", { staticClass: "mt-2" }, [
              e._v(e._s(e.conversationData.conversationWith.fullName))
            ]),
            r("br"),
            r("span", { staticClass: "text-muted" }, [
              e._v(e._s(e.conversationLastMessage.text))
            ])
          ]),
          r(
            "div",
            { staticClass: "messageDetails d-inline-block text-right" },
            [
              r("span", { staticClass: "mt-2 text-muted" }, [
                e._v(e._s(e.conversationLastMessage.at))
              ]),
              r("br"),
              r("span", { staticClass: "text-muted" }, [
                e._v(e._s(e.conversationLastMessage.seen ? "Vu" : ""))
              ])
            ]
          )
        ]);
      },
      xe = [],
      Je = {
        name: "Conversations",
        props: { conversationData: Object },
        data: function() {
          return {
            showPuctures: !1,
            conversationLastMessage: { text: "", at: 0, seen: !1 }
          };
        },
        mounted: function() {
          setTimeout(
            function(e) {
              e.showPuctures = !0;
            },
            100,
            this
          ),
            this.loadConversation();
        },
        methods: {
          loadConversation: function() {
            var e = this.conversationData.messages.sort(function(e, t) {
                return t.at - e.at;
              }),
              t = e[0];
            this.conversationLastMessage = {
              text: this.shortText(t.message),
              at: this.getStringDate(t.at),
              seen: t.seen
            };
          },
          getStringDate: function(e) {
            var t = new Date(e);
            return (
              t.getDate() +
              "/" +
              t.getMonth() +
              "/" +
              t.getFullYear() +
              " " +
              t.getHours() +
              "h" +
              t.getMinutes()
            );
          },
          shortText: function(e) {
            return e.substring(0, 20) + "...";
          }
        }
      },
      Ge = Je,
      Se = (s("d7b4"), Object(v["a"])(Ge, Ue, xe, !1, null, "3e235e0a", null)),
      Me = Se.exports,
      ye = function() {
        var e = this,
          t = e.$createElement,
          r = e._self._c || t;
        return r(
          "div",
          { staticClass: "conversationMessage" },
          [
            e._l(e.messages, function(e, t) {
              return r("MessageBuble", { key: t, attrs: { message: e } });
            }),
            r("br"),
            r("br"),
            r("br"),
            r(
              "div",
              {
                class:
                  "mesageWriteZone baseElement fixed-bottom p-3" +
                  (e.textWriten ? " showSendBtn" : "")
              },
              [
                r("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: e.newMessageText,
                      expression: "newMessageText"
                    }
                  ],
                  staticClass: "form-control messageInput d-inline-block",
                  attrs: { placeholder: "Saisissez votre message" },
                  domProps: { value: e.newMessageText },
                  on: {
                    input: function(t) {
                      t.target.composing || (e.newMessageText = t.target.value);
                    }
                  }
                }),
                e.textWriten
                  ? r("div", { staticClass: "d-inline-block sendMessage" }, [
                      r("img", { attrs: { src: s("64ec") } })
                    ])
                  : e._e()
              ]
            )
          ],
          2
        );
      },
      ke = [],
      je = function() {
        var e = this,
          t = e.$createElement,
          s = e._self._c || t;
        return s("div", [
          s(
            "div",
            {
              class:
                "m-3 message" + (e.isMessageSent ? " toRight messageSent" : "")
            },
            [
              s("div", { staticClass: "buble p-2" }, [
                e._v(e._s(e.message.message))
              ]),
              s(
                "div",
                {
                  class:
                    "text-muted mt-1 d-inline-block messageDetails" +
                    (e.isMessageSent ? " detail-right" : "")
                },
                [
                  s("span", { staticClass: "sendingDate" }, [
                    e._v(e._s(e.sendingDate) + " ")
                  ]),
                  e.message.seen && e.isMessageSent
                    ? s("span", { staticClass: "seen" }, [e._v("Vu")])
                    : e._e()
                ]
              )
            ]
          )
        ]);
      },
      Oe = [],
      Ee = {
        name: "MessageBuble",
        props: { message: Object },
        data: function() {
          return { isMessageSent: !1, sendingDate: "" };
        },
        mounted: function() {
          0 ===
            this.message.sender.uuid.localeCompare(
              window.schooler.userData.user.uuid
            ) && (this.isMessageSent = !0),
            (this.sendingDate = this.getStringDate(this.message.at));
        },
        methods: {
          getStringDate: function(e) {
            var t = new Date(e);
            return (
              t.getDate() +
              "/" +
              t.getMonth() +
              "/" +
              t.getFullYear() +
              " " +
              t.getHours() +
              "h" +
              t.getMinutes()
            );
          }
        }
      },
      qe = Ee,
      Te = (s("1885"), Object(v["a"])(qe, je, Oe, !1, null, "253b16e6", null)),
      Be = Te.exports,
      $e = s("1771"),
      Le = $e.Commons,
      We = {
        name: "ConversationMessages",
        components: { MessageBuble: Be },
        props: { conversation: Object },
        data: function() {
          return { messages: [], newMessageText: "", textWriten: !1 };
        },
        mounted: function() {
          this.loadMessages(),
            setInterval(
              function(e) {
                e.textWriten = !Le.isEmpty(e.newMessageText);
              },
              500,
              this
            );
        },
        methods: {
          loadMessages: function() {
            var e = this.conversation.messages.sort(function(e, t) {
              return t.at - e.at;
            });
            this.messages = e;
          }
        }
      },
      Re = We,
      ze = (s("a911"), Object(v["a"])(Re, ye, ke, !1, null, "422829b8", null)),
      Ie = ze.exports,
      Fe = s("1771"),
      He = Fe.Commons,
      Ae = {
        name: "Messages",
        components: { Conversations: Me, ConversationMessages: Ie },
        data: function() {
          return {
            conversations: [],
            conversationToShow: {},
            showMessages: !1
          };
        },
        mounted: function() {
          this.loadMessages();
        },
        methods: {
          loadMessages: (function() {
            var e = Object(o["a"])(
              regeneratorRuntime.mark(function e() {
                return regeneratorRuntime.wrap(
                  function(e) {
                    while (1)
                      switch ((e.prev = e.next)) {
                        case 0:
                          if (
                            !window.schooler.userData ||
                            He.isEmpty(window.schooler.userData.conversations)
                          ) {
                            e.next = 4;
                            break;
                          }
                          return (
                            console.log("conversations already exist"),
                            (this.conversations =
                              window.schooler.userData.conversations),
                            e.abrupt("return")
                          );
                        case 4:
                          (window.schooler.userData.conversations = s("6887")),
                            (this.conversations =
                              window.schooler.userData.conversations);
                        case 6:
                        case "end":
                          return e.stop();
                      }
                  },
                  e,
                  this
                );
              })
            );
            function t() {
              return e.apply(this, arguments);
            }
            return t;
          })(),
          showConversation: function(e) {
            (this.conversationToShow = this.conversations[e]),
              this.switchMessageShowing();
          },
          switchMessageShowing: function() {
            (window.schooler.userData.hideNavBar = !window.schooler.userData
              .hideNavBar),
              (this.showMessages = !this.showMessages);
          }
        }
      },
      Ye = Ae,
      Ve = (s("97bc"), Object(v["a"])(Ye, Ce, Pe, !1, null, "50b525f2", null)),
      Qe = Ve.exports,
      Ze = [
        { path: "/", component: L },
        { path: "/signup", component: H },
        { path: "/publications", component: oe },
        { path: "/users", component: Ne },
        { path: "/messages", component: Qe }
      ],
      Ke = Ze;
    s("ed18").config(),
      r["default"].use(n["a"]),
      r["default"].use(a["a"]),
      r["default"].use(O["a"]);
    var Xe = new O["a"]({ routes: Ke });
    new r["default"]({
      router: Xe,
      el: "#app",
      render: function(e) {
        return e(j);
      }
    });
  },
  "64e6": function(e) {
    e.exports = JSON.parse(
      '[{"sender":{"fullName":"John Doe","role":"Directeur","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Les devoirs on été programés, mais ne venez pas si vous n\'avez pas soldé la scolarité. soyez tous munis de vos cartes car nous allons être rigoureux sur la vérification.","picturesLinks":["s"],"filesLinks":[],"at":1623381886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Directeur","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"J\'espere que vous avez tous déposé vos mémoire, le délais est déja passé","picturesLinks":[],"filesLinks":[],"at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Directeur","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","picturesLinks":[],"filesLinks":[],"at":1623081886533,"seen":true}]'
    );
  },
  "64ec": function(e, t, s) {
    e.exports = s.p + "img/sendMessage.05c7bce3.png";
  },
  6887: function(e) {
    e.exports = JSON.parse(
      '[{"conversationWith":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"messages":[{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Bonjour Mr, j\'espere que o","at":1623381886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623381886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1622381886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1622381886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1622381886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1622381886533,"seen":false}]},{"conversationWith":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"messages":[{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Bonjour Mr, j\'espere que bous avez bien reçu les documents que j\'ai envoyé","at":1623381886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Bonjour","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Les documents ont été récupéré","at":1623081886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Je te fais signe ds que je finis de les examiner","at":1622381886533,"seen":false},{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"Merci beaucoup. Passsez une bonne journée","at":1622381886533,"seen":false}]},{"conversationWith":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"messages":[{"recipient":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"sender":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623381886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623380886533,"seen":false},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1623081886533,"seen":true},{"sender":{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},"recipient":{"fullName":"John Doe Géraud","uuid":"GéraudUuid","role":"3eme année licence","picture":1},"message":"prépare bien les documents qu\'il faut","at":1622381886533,"seen":false}]}]'
    );
  },
  "742e": function(e, t, s) {
    e.exports =
      s.p + "img/130188658_690024375233857_106948469018641771_n.e3fa2d0f.jpg";
  },
  "760c": function(e, t, s) {
    "use strict";
    s("c3f5");
  },
  "765b": function(e, t, s) {
    "use strict";
    s("3e82");
  },
  8060: function(e, t, s) {
    "use strict";
    s("36e8");
  },
  "85ec": function(e, t, s) {},
  "85f4": function(e, t, s) {},
  "86bd": function(e, t, s) {},
  "8efa": function(e, t, s) {},
  "97bc": function(e, t, s) {
    "use strict";
    s("86bd");
  },
  "9d4d": function(e, t, s) {},
  a278: function(e, t, s) {
    var r = { "./userPicture_1.png": "d734" };
    function n(e) {
      var t = a(e);
      return s(t);
    }
    function a(e) {
      if (!s.o(r, e)) {
        var t = new Error("Cannot find module '" + e + "'");
        throw ((t.code = "MODULE_NOT_FOUND"), t);
      }
      return r[e];
    }
    (n.keys = function() {
      return Object.keys(r);
    }),
      (n.resolve = a),
      (e.exports = n),
      (n.id = "a278");
  },
  a638: function(e) {
    e.exports = JSON.parse(
      '[{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe","role":"Sécrétaire","uuid":"secretaireUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1},{"fullName":"John Doe Géraud","role":"3eme année licence","uuid":"GéraudUuid","picture":1}]'
    );
  },
  a911: function(e, t, s) {
    "use strict";
    s("01d1");
  },
  b7da: function(e, t, s) {},
  b9f3: function(e, t, s) {
    "use strict";
    s("9d4d");
  },
  be05: function(e, t, s) {},
  c3f5: function(e, t, s) {},
  d734: function(e, t, s) {
    e.exports = s.p + "img/userPicture_1.9195b8fe.png";
  },
  d7b4: function(e, t, s) {
    "use strict";
    s("be05");
  },
  da41: function(e, t, s) {
    "use strict";
    s("266e");
  },
  f800: function(e, t, s) {
    "use strict";
    s("8efa");
  },
  f83a: function(e, t, s) {
    e.exports = s.p + "img/messages.ab291be6.png";
  }
});
//# sourceMappingURL=app.72c9c949.js.map
