import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import App from "./App.vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import VueRouter from "vue-router";
var VueCookie = require("vue-cookie");
import VueMeta from 'vue-meta';

require("dotenv").config();

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueRouter);
Vue.use(VueCookie);
Vue.use(VueMeta);

import routes from "./utils/routes";

const router = new VueRouter({
  routes // short for `routes: routes`
});

new Vue({
  router,
  el: "#app",
  render: h => h(App)
});
