import Login from "../pages/Login.vue";
import Signup from "../pages/Signup.vue";
import Publications from "../pages/Publications.vue";
import Users from "../pages/Users.vue";
import Messages from "../pages/Messages.vue";

const routes = [
  { path: "/", component: Login },
  { path: "/signup", component: Signup },
  { path: "/publications", component: Publications },
  { path: "/users", component: Users },
  { path: "/messages", component: Messages }
];

export default routes;
