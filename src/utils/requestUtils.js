const axios = require("axios");
const { Commons } = require("./commons");
class RequestUtils {
  static async getDataFrom(url, header = null) {
    return request("get", url, header, null, null);
  }
  static async sendRequestTo(url, method, header, data) {
    return request(method, url, header, data);
  }
}

async function request(
  method,
  url,
  headers = null,
  data = null,
  responseType = null
) {
  const requestConfig = {
    url,
    method
  };
  Commons.isEmpty(headers) ? null : (requestConfig.headers = headers);
  Commons.isEmpty(data) ? null : (requestConfig.data = data);
  Commons.isEmpty(responseType)
    ? null
    : (requestConfig.responseType = responseType);

  const response = await axios.request(requestConfig).catch(error => {
    // if (Commons.isProductionEnvironment()) {
    //   Sentry.captureException(new Error(error));
    // } else {
    console.log(`Failed caused by access to : ${url} with error :  ${error}`);
    console.log(error.message);
    // }

    return error.response || error;
  });

  return response.data || response;
}

exports.RequestUtils = RequestUtils;
