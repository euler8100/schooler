class Commons {
    static initGlobalVariables() {
        // const apiBaseUrl = "http://localhost:3008";
        // const apiBaseUrl = "https://3008-kumquat-thrush-qi7kkjih.ws-eu09.gitpod.io";
        const apiBaseUrl = "https://schooler-api.herokuapp.com";
        window.schooler = {
            apiServerBaseUrl: apiBaseUrl,
            apiEndpoints: {
                signUpUrl: apiBaseUrl + "/user/signup",
                userAcceptationUrl: apiBaseUrl + "/user/accept",
                loginUrl: apiBaseUrl + "/user/login",
                usersUrl: apiBaseUrl + "/user",
                publicationsUrl: apiBaseUrl + "/publications",
                getConversationsUrl: apiBaseUrl + "/message/last/100",
                getMessageUrl: apiBaseUrl + "/message/:bynomeUuid",
                sendMessageUrl: apiBaseUrl + "/message/send"
            },
            userData: {
                user: {},
                userConnected: false
            },
            users: [],
            conversations: [],
            messages: {},
            alertData: {
                messageTitle: "",
                message: "",
                greenBtn: false,
                greenBtnMessage: "",
                greenBtnAction: null,
                redBtn: false,
                redBtnMessage: "",
                redBtAction: null
            },
            userMessageToShow: {}
        }
    }

    static sendAlert(title,
        message,
        successFullAlert = true,
        showGreenButton = false,
        greenButtonText = "",
        greenButtonAction = function () { },
        showRedButton = false,
        redButtonText = "",
        redButtonAction = function () { }) {
        const messageTitle = title || (successFullAlert ? "Succès" : "Erreur");
        window.schooler.alertData.messageTitle = messageTitle,
            window.schooler.alertData.message = message,
            window.schooler.alertData.greenBtn = showGreenButton,
            window.schooler.alertData.greenBtnMessage = greenButtonText,
            window.schooler.alertData.greenBtnAction = greenButtonAction,
            window.schooler.alertData.redBtn = showRedButton,
            window.schooler.alertData.redBtnMessage = redButtonText,
            window.schooler.alertData.redBtnAction = redButtonAction,
            document.querySelector("#alertBtn").click()
    }

    static isEmpty(data) {
        if (!data) {
            return true;
        }
        if (Array.isArray(data)) {
            return data.length === 0;
        }
        const dataProperties = Object.values(data);
        return dataProperties && dataProperties.length == 0;
    }

    static async waitUntil(conditionFunction, callbackSuccess = () => { }) {
        if (conditionFunction()) {
            callbackSuccess();
            return;
        }

        return await new Promise((resolve) => {
            const interval = setInterval(() => {
                if (conditionFunction()) {
                    callbackSuccess();
                    resolve('');
                    clearInterval(interval);
                }
            }, 100);
        });
    }

    static getSearchRegexFrom(string) {
    return new RegExp(`${string
      .replace(/[aáàäâ]/gi, '[aáàäâ]')
      .replace(/[eèéëê]/gi, '[eèéëê]')
      .replace(/[iíïî]/gi, '[iíïî]')
      .replace(/[oóöòô]/gi, '[oóöòô]')
      .replace(/[uüúùû]/gi, '[uüúùû]')
      .replace(/\./gi, '.?')
      .replace(/[ :,’'-]/gi, '.?')}`,"i")
  }
}
exports.Commons = Commons;
