const { Commons } = require("./commons");
const { RequestUtils } = require("./requestUtils");

class DataUtils {
  static async loadUsers() {
    const usersResponse = await RequestUtils.getDataFrom(
      window.schooler.apiEndpoints.usersUrl,
      { userUuid: window.schooler.userData.user.uuid }
    );
    if (!Commons.isEmpty(usersResponse.error)) {
      Commons.sendAlert(
        "",
        "Une erreur s'est produite, veuillez recharger la page s'il vous plait"
      );
      return;
    }
    if (!usersResponse.success) {
      Commons.sendAlert("", usersResponse.message, false);
      return;
    }
    console.log(usersResponse.data.users);
    let users = usersResponse.data.users.filter(
      user =>
        user.uuid.localeCompare(window.schooler.userData.user.uuid) !== 0 &&
        (user.role.localeCompare("Etudiant") !== 0 ||
          window.schooler.userData.user.role.localeCompare("Etudiant") !== 0)
    );
    const acceptedUsers = users.filter(user => user.isAccepted);
    const notAcceptedUsers = users.filter(user => !user.isAccepted);

    if (window.schooler.userData.isAdmin) {
      window.schooler.users = [...notAcceptedUsers, ...acceptedUsers];
    } else {
      window.schooler.users = acceptedUsers;
    }
  }

  static async loadConversations() {
    const conversationResponse = await RequestUtils.getDataFrom(
      window.schooler.apiEndpoints.getConversationsUrl,
      { userUuid: window.schooler.userData.user.uuid }
    );
    console.log(conversationResponse);
    if (!Commons.isEmpty(conversationResponse.error)) {
      Commons.sendAlert(
        "",
        "Une erreur s'est produite, veuillez recharger la page s'il vous plait"
      );
      return;
    }
    if (!conversationResponse.success) {
      Commons.sendAlert("", conversationResponse.message, false);
      return;
    }
    const conversationList = conversationResponse.data.lastsMessages
      .map(conversationMessage => buildConversationFrom(conversationMessage))
      .sort((conversationMessageA, conversationMessageB) => {
        return conversationMessageB.sentAt - conversationMessageA.sentAt;
      });
    window.schooler.conversations = conversationList;
  }

  static async loadMessagesWith(conversationBynome) {
    const messageResponse = await RequestUtils.getDataFrom(
      window.schooler.apiEndpoints.getMessageUrl.replace(
        /:bynomeUuid/,
        conversationBynome.uuid
      ),
      { userUuid: window.schooler.userData.user.uuid }
    );
    if (!Commons.isEmpty(messageResponse.error)) {
      Commons.sendAlert(
        "",
        "Une erreur s'est produite, veuillez recharger la page s'il vous plait"
      );
      return;
    }
    if (!messageResponse.success) {
      Commons.sendAlert("", messageResponse.message, false);
      return;
    }
    const orderedMessages = messageResponse.data.messages.sort(
      (messageA, messageB) => {
        return messageB.sentAt - messageA.sentAt;
      }
    );
    window.schooler.messages[conversationBynome.uuid] = {
      conversationBynome,
      orderedMessages
    };
  }

  static async sendMessage(recipientUuid,message){
      const messageResponse = await RequestUtils.sendRequestTo(
        window.schooler.apiEndpoints.sendMessageUrl,
        "post",
        { userUuid: window.schooler.userData.user.uuid },
        {
          "recipientUuid":recipientUuid,
          "message":message
        }
      )
      console.log(messageResponse)
      if (!Commons.isEmpty(messageResponse.error)) {
        Commons.sendAlert(
          "",
          "Une erreur s'est produite, veuillez recharger la page s'il vous plait"
        );
        return;
      }
      if (!messageResponse.success) {
        Commons.sendAlert("", messageResponse.message, false);
        return;
      }
  }

  static findUser(userUuid){
      const users = [{
        uuid:"publicationGroupe"
    },window.schooler.userData.user,...window.schooler.users];
  const user = users.find(
    user =>
      user.uuid.localeCompare(userUuid) === 0
  );
  return user
  }
}

function buildConversationFrom(conversationMessage) {
    const conversationBynome = DataUtils.findUser(conversationMessage.conversationBinomeUuid)
  return {
    conversationBynome,
    ...conversationMessage.message
  };
}

exports.DataUtils = DataUtils;
